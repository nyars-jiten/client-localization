import { EditsType } from "@/api/types";

export default {
    loading: "Загрузка",
    menu: {
        home: "Главная",
        thisDict: {
            name: "Эта статья",
            dropdown: {
                edit: "Изменить",
                stress: "Ударение",
                remove: "Удалить",
            },
        },
        dict: {
            name: "Словарь",
            dropdown: {
                addWrod: "Добавить статью",
                subjects: "Тематики",
                edits: "Правки",
                lastEdits: "Правки",
                tags: "Теги",
                viewAllTags: "Показать всё",
            },
        },
        forum: {
            name: "Форум",
            dropdown: {
                sections: "Категории",
            },
        },
        downloads: { name: "Загрузки" },
        sfx: { name: "Звуки" },
        community: {
            name: "Комьюнити",
            dropdown: {
                signIn: "Вход",
                signOut: "Выход",
            },
        },
    },
    edits: {
        comments: {
            drop: "Ваш комментарий тут",
            dropped: " оставил(а) комментарий",
            save: "Сохранить",
            edited: "изменён",
            noComments: "Нет комментариев.",
        },
    },
    dict: {
        accent: {
            add: "Добавить",
            save: "Сохранить",
            reading: "Чтение",
            accent: "Ваш текст тут",
        },
        home: {
            lastRequests: "Недавно искали",
            draw: "Рисовать",
            blocks: {
                editsLast: {
                    name: {
                        text: "Правки",
                        help1: "・ Вы можете помочь нам расширить словарь ",
                        help2: "вот тут.",
                    },
                    corpus: {
                        nyars: { value: "和露", desc: "Корпус японского языка" },
                        rowa: { value: "ря", desc: "Корпус русского языка" },
                        hz: { value: "хз", desc: "" },
                    },
                    types: {
                        [EditsType.newWord]: "добавил(а) статью",
                        [EditsType.editWord]: "отредактировал(а) статью",
                        [EditsType.deleteWord]: "предложил(а) удалить статью",
                        [EditsType.editKanji]: "EditsType.editKanji",
                    },
                    updated: "Обновлено",
                    more: "Подробнее",
                    notUpdated: "Не обновлялось",
                },
            },
        },
        view: {
            sound: "Воспроизвести",
            soundSamples: "Примеры",
            noSound: "Найти пример",
            accent: "Ударения",
            kanji: {
                detailInfo: {
                    classifications: "Классификация",
                    indeces: "Индексы",
                },
                strokeCount0: "черт",
                strokeCount1: "черта",
                strokeCount2: "черты",
                radical: "Радикал",
                meanings: "Значения",
                strokeAdditional: "Дополнительные варианты написания",
            },
            // Указатели в словарях
        },
        search: {
            corpus: {
                nyars: "Японско-русский словарь",
                kanji: "Словарь иероглифов",
                rowa: "Русско-японский словарь",
            },
            noResult: "Ничего не найдено",
        },
    },
    users: {
        page: {
            rating: {
                summary: {
                    categories: {
                        period: "Период",
                        newWords: "Статьи",
                        edits: "Правки",
                        examples: "Примеры",
                        points: "Рейтинг",
                    },
                    periods: {
                        alltime: "За всё время",
                        month: "За месяц",
                        week: "За неделю",
                        today: "За сегодня",

                        alltimeRating: "За всё время",
                        monthRating: "За месяц",
                        weekRating: "За неделю",
                        todayRating: "За сегодня",
                    },
                },
            },
        },
    },
};
